import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Login from '@/components/Login'
import Signup from '@/components/Signup'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('authToken') !== null || ['/login', '/signup'].includes(to.path)) {
    next()
  } else {
    next('/login')
  }
})

export default router
