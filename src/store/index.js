import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import auth from './modules/auth'
import session from './modules/session'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    auth,
    session
  },
  state: {
    menuVisible: false,
    errors: []
  },
  getters: {
    getMenuVisible: state => state.menuVisible,
    getErrors: state => state.errors
  },
  mutations: {
    toggleMenu(state) {
      state.menuVisible = !state.menuVisible
    },
    updateErrors(state, errors) {
      state.errors = errors
    }
  }
})
