import { SESSION_REQUEST, SESSION_CREATE, SESSION_SUCCESS, SESSION_ERROR, SESSION_SELECT, SESSION_SEARCH, MESSAGE_CREATE } from '../types'
import axios from 'axios'
import _ from 'lodash'

const state = {
  sessions: [],
  selectedSession: null,
  isLoading: false
}

const getters = {
  isSessionLoading: state => state.isLoading,
  getSessions: state => state.sessions,
  getSelectedSession: state => state.selectedSession,
  getUnreadSessionCount: (state) => {
    return _.filter(state.sessions, 'has_unread_message').length
  }
}

const actions = {
  [SESSION_REQUEST]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit(SESSION_REQUEST)
      axios({url: 'api/v1/sessions/', method: 'GET'})
        .then(resp => {
          commit(SESSION_SUCCESS, { sessions: resp.data.results })
          resolve(resp)
        })
        .catch(err => {
          commit(SESSION_ERROR)
          reject(err)
        })
    })
  },
  [SESSION_CREATE]: ({ commit, dispatch }, data) => {
      return new Promise((resolve, reject) => {
          commit(SESSION_REQUEST)
          axios({ url: 'api/v1/sessions/', data, method: 'POST'})
            .then(resp => {
                commit(SESSION_SUCCESS, { selectedSession: resp.data })
                return dispatch(SESSION_REQUEST)
            })
            .then(resp => {
                resolve(resp)
            })
            .catch(err => {
                commit(SESSION_ERROR)
                reject(err)
            })
      })
  },
  [SESSION_SEARCH]: ({ commit, dispatch }, searchTerm) => {
      return new Promise((resolve, reject) => {
          commit(SESSION_REQUEST)
          axios.get('api/v1/sessions', { params: { search: searchTerm }})
            .then(resp => {
                commit(SESSION_SUCCESS, { sessions: resp.data.results })
                resolve(resp)
            })
            .catch(err => {
                commit(SESSION_ERROR)
                reject(err)
            })
      })
  },
  [MESSAGE_CREATE]: ({ commit, dispatch, state }, data) => {
      return new Promise((resolve, reject) => {
          commit(SESSION_REQUEST)
          axios({ url: `api/v1/sessions/${state.selectedSession.id}/message/`, data, method: 'POST' })
            .then(resp => {
                commit(SESSION_SUCCESS, { selectedSession: resp.data })
                resolve(resp)
            })
            .catch(err => {
                commit(SESSION_ERROR)
                reject(err)
            })
      })
  },
  [SESSION_SELECT]: ({ commit, dispatch, state }, selectedSession) => {
      return new Promise((resolve, reject) => {
          commit(SESSION_REQUEST)
          axios({ url: `api/v1/sessions/${selectedSession.id}/messages/`, method: 'PUT' })
            .then(resp => {
                const index = _.findIndex(state.sessions, {id: resp.data.id})
                const new_sessions = [ ...state.sessions.slice(0, index), resp.data, ...state.sessions.slice(index + 1)]
                commit(SESSION_SUCCESS, { sessions: new_sessions, selectedSession: resp.data })
                resolve(resp)
            })
            .catch(err => {
                reject(err)
            })
      })
  }
}

const mutations = {
  [SESSION_REQUEST]: (state) => {
    state.isLoading = true
  },
  [SESSION_SUCCESS]: (state, { sessions, selectedSession }) => {
    state.isLoading = false
    if (sessions) {
        state.sessions = sessions
    }
    if (selectedSession) {
        state.selectedSession = selectedSession
    }
  },
  [SESSION_ERROR]: (state) => {
    state.isLoading = false
  },
  [SESSION_SELECT]: (state, selectedSession) => {
    state.selectedSession = selectedSession
  },
  [SESSION_CREATE]: (state) => {
      state.selectedSession = null
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
