import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT, AUTH_REFRESH, USER_REQUEST } from '../types'
import axios from 'axios'

const state = {
  token: localStorage.getItem('authToken') || '',
  status: '',
  hasLoadedOnce: false
}

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  getToken: state => state.token
}

const actions = {
  [AUTH_REQUEST]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)
      axios({url: 'auth/jwt/create/', data: user, method: 'POST'})
        .then(resp => {
          commit(AUTH_SUCCESS, resp.data.token)
          return dispatch(USER_REQUEST)
        })
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
          reject(err)
        })
    })
  },
  [AUTH_REFRESH]: ({commit, dispatch, state}) => {
    axios.defaults.headers.common['Authorization'] = `jwt ${state.token}`
    return new Promise((resolve, reject) => {
      dispatch(USER_REQUEST)
        .then(resp => {
          commit(AUTH_SUCCESS, state.token)
          resolve(resp)
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
          reject(err)
        })
    })
  },
  [AUTH_LOGOUT]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      resolve()
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [AUTH_SUCCESS]: (state, token) => {
    state.status = 'success'
    state.token = token
    localStorage.setItem('authToken', token)
    axios.defaults.headers.common['Authorization'] = `jwt ${token}`
    state.hasLoadedOnce = true
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
    delete axios.defaults.headers.common.Authorization
    localStorage.removeItem('authToken')
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
    delete axios.defaults.headers.common.Authorization
    localStorage.removeItem('authToken')
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
