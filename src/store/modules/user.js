import { USER_REQUEST, USER_ERROR, USER_SUCCESS, USER_SIGNUP, USER_SEARCH, USER_RESET_SEARCH, AUTH_LOGOUT } from '../types'
// import Vue from 'vue'
import axios from 'axios'

const state = { status: '', profile: {}, userSearchResults: [] }

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile.name,
  getRsps: state => state.profile.report_source_profiles,
  getUserSearchResults: state => state.userSearchResults
}

const actions = {
  [USER_REQUEST]: ({commit, dispatch}) => {
    commit(USER_REQUEST)
    return new Promise((resolve, reject) => {
      axios({url: 'auth/me/'})
        .then(resp => {
          commit(USER_SUCCESS, { user: resp.data })
          resolve(resp.data)
        })
        .catch(resp => {
          commit(USER_ERROR)
          dispatch(AUTH_LOGOUT)
          reject(resp)
        })
    })
  },
  [USER_SIGNUP]: ({ commit, dispatch }, user) => {
    commit(USER_REQUEST)
    return new Promise((resolve, reject) => {
      axios({ url: 'auth/users/create/', data: user, method: 'POST' })
        .then(resp => {
          commit(USER_SUCCESS, { user: resp.data })
          resolve(resp)
        })
        .catch(err => {
          commit(USER_ERROR)
          reject(err)
        })
    })
  },
  [USER_SEARCH]: ({ commit, dispatch }, searchTerm) => {
    commit(USER_REQUEST)
    return new Promise((resolve, reject) => {
      axios.get('api/v1/users', { params: { search: searchTerm } })
        .then(resp => {
          commit(USER_SUCCESS, { userSearchResults: resp.data.results })
          resolve(resp)
        })
        .catch(err => {
          commit(USER_ERROR)
          reject(err)
        })
    })
  }
}

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, { user, userSearchResults }) => {
    state.status = 'success'
    if (user) {
      state.profile = user
    }
    if (userSearchResults) {
      state.userSearchResults = userSearchResults
    }
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  },
  [USER_RESET_SEARCH]: (state) => {
    state.userSearchResults = []
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
