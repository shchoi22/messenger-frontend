// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import axios from 'axios'

import moment from 'moment'
import 'moment/locale/ko'
moment.locale('ko')

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
Vue.use(VueMaterial)

Vue.config.productionTip = process.env.NODE_ENV === 'production'
axios.defaults.baseURL = process.env.API_BASE_URL


Vue.filter('formatDate', function (value) {
  let modified_date = moment(value)

  if (moment().diff(modified_date, 'days') >= 1){
    return modified_date.format('LT')
  } else {
    return modified_date.format('l')
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

